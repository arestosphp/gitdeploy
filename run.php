<?php
if(!file_exists(__DIR__.'/config.php')){
    echo 'config.php not found! Please create it by copy from config.sample.php!'.PHP_EOL;
    exit;
}
$config=include __DIR__.'/config.php';
date_default_timezone_set('Asia/Hong_Kong');
$deployLocations=$config['locations'];
$lock=abs(intval($config['timeout']));
if(!$lock) $lock=500;
$lockFile=__DIR__.'/data/run.lock';
if(file_exists($lockFile)){
    $lastRun=filemtime($lockFile);
    if(time()-$lastRun<$lock){
        $remaining=$lock-(time()-$lastRun);
        echo 'Already running. '.$remaining.' seconds remaining!';
        exit;
    }
}
file_put_contents($lockFile,'1');
$additional_commands=$config['commands']??[];
$branchesToDeploy=array_keys($deployLocations);
foreach ($branchesToDeploy as $branch) {
    $now=date('Y-m-d H:i:s');
	echo "[$now] Deploy starting for branch [$branch]" . PHP_EOL;
	if (!isset($deployLocations[$branch])) {
		echo 'No deploy location found' . PHP_EOL;
		continue;
	}
	$deployLocation = $deployLocations[$branch];
    $location = $deployLocation['git'];

	if (!file_exists($location) || !is_dir($location)) {
		echo $location . ' not exists' . PHP_EOL;
		continue;
	}
    $location = realpath($location);
    $versionFile = $deployLocation['version.txt']??'';
    if(!$versionFile){
        $versionFile=$location.'/version.txt';
    }elseif(substr($versionFile,0,1)!=='/'){
        $versionFile=$location.'/'.$versionFile;
    }
    #check if local branch matches remote branch
    $local_branch = trim(shell_exec("cd $location && git rev-parse --abbrev-ref HEAD"));
    if ($local_branch !== $branch) {
        echo 'Local branch ' . $local_branch . ' does not match remote branch ' . $branch . PHP_EOL;
        #force checkout to remote branch
        $checkout_command = "git checkout -f $branch";
        shell_exec("cd $location && $checkout_command");
        #reset hard to remote branch
        $reset_command = "git reset --hard origin/$branch";
        shell_exec("cd $location && $reset_command");
    }
    #compare local head with remote head
    $local_head = trim(shell_exec("cd $location && git rev-parse HEAD"));
    $remote_head = trim(shell_exec("cd $location && git ls-remote origin -h refs/heads/$branch | cut -f1"));
    $local_head_short = substr($local_head, 0, 7);
    $remote_head_short = substr($remote_head, 0, 7);
    $now=date('Y-m-d H:i:s');
    if ($local_head === $remote_head) {
        echo "Local head matches remote head [$remote_head]" . PHP_EOL;
        if(!file_exists($versionFile)){
            file_put_contents($versionFile,"[$now]".' Latest version already deployed '.$local_head_short);
        }
        continue;
    }
	echo "Deploy [$remote_head_short] from [$branch] to [$location]". PHP_EOL;
	$deploy_command = "git reset --hard origin/$branch && git clean -fd && git pull";
	$commands = array_merge([$deploy_command], $additional_commands);

	$result = "[$now]".' Deploy ' . $branch . '=> '.$location  . PHP_EOL;
    $result .= $local_head_short . ' => ' . $remote_head_short . PHP_EOL;
	$result .= '===================================' . PHP_EOL . PHP_EOL;
	foreach ($commands as $cmd) {
		$command = "cd $location && " . $cmd;
		$result .= $command . PHP_EOL;
		$result .= shell_exec($command);
		$result .= PHP_EOL . PHP_EOL;

	}
	file_put_contents($versionFile, $result);
}
unlink($lockFile);
