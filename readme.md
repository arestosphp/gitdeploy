# Auto Deployment from git script
1.  Update config.php
- locations: map from branch to deploy location
- timeout: wait time for next run
- commands: additional commands eg: composer update
2. Create cronjob to run.php every minutes
2. Make sure data directory writeable


```cronexp
* * * * * php ~/gitdeploy/run.php >/dev/null 2>&1
```

