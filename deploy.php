<?php
die('Deprecated this file is not used anymore');
$config=include __DIR__.'/config.php';
$payload=file_get_contents('php://input');
$branch='';
if($payload){
    $payloadData=json_decode($payload,true);
    if(isset($payloadData['push']['changes'][0]['new']['name'])){
        $branch=$payloadData['push']['changes'][0]['new']['name'];
    }
    if(isset($payloadData['pullrequest']['destination']['branch']['name'])){
        $branch=$payloadData['pullrequest']['destination']['branch']['name'];
    }
}else{
	die('No payload data');
}
if($branch){
	if(isset($config['locations'][$branch])) {
		file_put_contents(DATA_DIR.'/'.$branch.'.json','1');
		echo "Deploy for $branch will start soon";
	}
}